<?php

namespace App\Repositories;

use App\Interfaces\CounterInterface;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

class HitRepository extends DocumentRepository implements CounterInterface
{
    /**
     * @param string $field
     * @param string $value
     * @return int
     * @throws MongoDBException
     */
    public function countByFieldFilter(string $field, string $value): int
    {
        $qb = $this->createQueryBuilder()
            ->field($field)->equals($value)
            ->count()
        ;

        return $qb->getQuery()->execute();
    }
}