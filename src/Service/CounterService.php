<?php

namespace App\Service;

use App\Interfaces\CounterInterface;
use Exception;

class CounterService
{
    /**
     * @param string $field
     * @param string $value
     * @param CounterInterface $counter
     * @return int|string
     */
    public function getCount(string $field, string $value, CounterInterface $counter)
    {
        try {
            $linksCount = $counter->countByFieldFilter($field, $value);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

        return $linksCount;
    }
}