<?php

namespace App\Interfaces;

interface CounterInterface

{
    /**
     * Returns how many $field has $value
     * @param string $field
     * @param string $value
     */
    public function countByFieldFilter(string $field, string $value);
}