<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Index;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @Document(db="heatmap", collection="myHits", repositoryClass="App\Repositories\HitRepository")
 */
class Hit
{
    public const PRODUCT = "product";
    public const CATEGORY = "category";
    public const STATIC_PAGE = "static-page";
    public const CHECKOUT = "checkout";
    public const HOMEPAGE = "homepage";

    /**
     * @Id(type="string", strategy="AUTO")
     * @OA\Property(description="The unique identifier")
     */
    private string $id;

    /**
     * @Field(type="string")
     * @OA\Property(type="string")
     * @Groups("in", "count")
     */
    private string $link;

    /**
     * @Field(type="string")
     * @OA\Property(type="string")
     * @Groups("in", "count")
     */
    private string $linkType;

    /**
     * @Field(type="string")
     * @OA\Property(type="string")
     * @Groups("in")
     */
    private string $timestamp;

    /**
     * @Field(name="customer_id", type="int")
     * @Index(order="asc")
     * @OA\Property(type="integer")
     * @Groups("in")
     */
    private int $customerId;

    /**
     * @param string $id
     * @return Hit
     */
    public function setId(string $id): Hit
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Hit
     */
    public function setLink(string $link): self
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkType(): string
    {
        return $this->linkType;
    }

    /**
     * @param string $linkType
     * @return Hit
     */
    public function setLinkType(string $linkType): self
    {
        $this->linkType = $linkType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     * @return Hit
     */
    public function setTimestamp(string $timestamp): self
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return Hit
     */
    public function setCustomerId(int $customerId): self
    {
        $this->customerId = $customerId;
        return $this;
    }
}