<?php

namespace App\Controller;

use App\Service\CounterService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use App\Document\Hit;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\SerializerInterface;

class HeatmapController extends AbstractController
{
    /**
     * @Route("/api/hits", methods={"POST"})
     * @OA\Response(
     *     response=201,
     *     description="Adds a new document with request parameters"
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @OA\JsonContent(
     *          ref=@Model(type=Hit::class, groups={"in"})
     *     )
     * )
     * @OA\Tag(name="hit")
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function fetchHitAction(Request $request, DocumentManager $documentManager, SerializerInterface $serializer): JsonResponse
    {
        try {
            $document = $serializer->deserialize($request->getContent(), Hit::class, 'json');
            $documentManager->persist($document);
            $documentManager->flush();
        } catch (Exception $exception) {
            return new JsonResponse($exception->getMessage(),400);
        }

        return new JsonResponse([],201);
    }

    /**
     * @Route("/api/hits", methods={"GET"})
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param CounterService $counterService
     * @return JsonResponse
     * @OA\Response(
     *     response=200,
     *     description="Gets number of hits for selected fields"
     * )
     * @OA\Parameter(in="query", name="link")
     * @OA\Parameter(in="query", name="linkType")
     * )
     * @OA\Tag(name="hit")
     */
    public function getHitsAction(Request $request, DocumentManager $documentManager, CounterService $counterService): JsonResponse
    {
        $hitRepository = $documentManager->getRepository(Hit::class);
        if ($request->query->has("link")) {
            $link = $request->query->get("link");
            $linksCount = $counterService->getCount('link', $link, $hitRepository);
            if (is_string($linksCount)) {
                return new JsonResponse($linksCount, Response::HTTP_NOT_FOUND);
            }
        }

        if ($request->query->has("linkType")) {
            $linkType = $request->query->get("linkType");
            $linksTypeCount = $counterService->getCount('linkType', $linkType, $hitRepository);
            if (is_string($linksTypeCount)) {
                return new JsonResponse($linksTypeCount, Response::HTTP_NOT_FOUND);
            }
        }

        $data = [];
        if (isset($linksCount)) {
            $data['links'] = $linksCount;
        }

        if (isset($linksTypeCount)) {
            $data['linksType'] = $linksTypeCount;
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }
}