Heatmap Project
=======
-------

A Symfony 5.3 Project with Docker and MongoDB

- Project setup:

1) go to docker folder 
2) run "docker-compose --build -d"
3) run "docker exec -it testPhpFpm sh"
4) run "composer install"

After a small delay, you can access the project on your browser at "http://localhost/api/doc" 